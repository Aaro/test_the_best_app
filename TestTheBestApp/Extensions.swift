//
//  Extensions.swift
//  TestTheBestApp
//
//  Created by Надежда Шальнова on 08.02.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

import UIKit

extension UITableViewCell{
    public static func createForTableView(_ tableView: UITableView) -> UITableViewCell? {
        let className = String(describing:self)
        var cell: UITableViewCell? = nil
        cell = tableView.dequeueReusableCell(withIdentifier: className)
        if (cell == nil) {
            cell = Bundle.main.loadNibNamed(className, owner: self, options: nil)?.first as? UITableViewCell
            let cellNib = UINib(nibName: className, bundle: nil)
            tableView.register(cellNib, forCellReuseIdentifier: className)
        }
        return cell
    }
}
