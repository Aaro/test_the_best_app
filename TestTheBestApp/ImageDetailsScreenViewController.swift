//
//  ImagePreviewViewController.swift
//  TestTheBestApp
//
//  Created by Надежда Шальнова on 10.02.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

import UIKit

class ImageDetailsScreenViewController: UIViewController {
    
    @IBOutlet private weak var nameImageLabel: UILabel!
    @IBOutlet private weak var dateImageLabel: UILabel!
    @IBOutlet private weak var tagsImageLabel: UILabel!
    @IBOutlet private weak var imagePreviewImageView: UIImageView!
    @IBOutlet private weak var backgroundImageView: UIImageView!
    var imageObject: ImageObject = ImageObject()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameImageLabel.text = self.imageObject.name
        dateImageLabel.text = self.dateToString(self.imageObject.date)
        tagsImageLabel.text = self.imageObject.tags
        ImageManager.loadImage(fromUrl: imageObject.imageUrl) { (image) in
            self.imagePreviewImageView.image = image
            self.backgroundImageView.image = image
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.title = self.imageObject.name
    }
    
    func dateToString(_ date: Date)->String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
}
