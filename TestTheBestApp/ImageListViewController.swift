//
//  ImageListViewController.swift
//  TestTheBestApp
//
//  Created by Надежда Шальнова on 08.02.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

import UIKit

class ImageListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate {
    @available(iOS 8.0, *)
    
    public func updateSearchResults(for searchController: UISearchController) {
        
        let usersString = self.searchController.searchBar.text
        let unsafeChars = NSCharacterSet.alphanumerics.inverted
        let result = usersString?.components(separatedBy: unsafeChars).filter { (checkedString) -> Bool in
            return !checkedString.isEmpty
            }.joined(separator: ",")
        self.tags = result!
    }
    
     // MARK: fields
    var imageObjectsArray = [ImageObject]()
    var tagsImageObjectsArray = [ImageObject]()
    @IBOutlet weak var imageTableView: UITableView!
    let cellReuseIdentifier = "cell"
    
    var searchController: UISearchController!
    var resultsController = UITableViewController()
    var tags: String = ""
    let hieghtCell: CGFloat = 500
    
    // MARK: methods
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchController.searchBar.endEditing(true)
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchController.searchBar.endEditing(true)
        self.downloadImageList(updateTableView: resultsController.tableView)
    }
    
    
    @IBAction func sortButton(_ sender: Any) {
        self.showSortAlert()
    }
    
    
    func sortName() {
        self.imageObjectsArray.sort {$0.name < $1.name}
    }
    
    
    func sortDate() {
        self.imageObjectsArray.sort {$0.date > $1.date}
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageTableView.delegate = self
        imageTableView.dataSource = self
        
        self.resultsController.tableView.delegate = self
        self.resultsController.tableView.dataSource = self
        
        self.searchController = UISearchController(searchResultsController: self.resultsController)
        self.imageTableView.tableHeaderView  = self.searchController.searchBar
        self.searchController.searchResultsUpdater = self
        self.searchController.searchBar.delegate = self
        
        self.downloadImageList(updateTableView: self.imageTableView)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 2.0/255.0, green: 96.0/255.0, blue: 132.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == self.imageTableView){
            return self.imageObjectsArray.count
        } else {
             return self.tagsImageObjectsArray.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var imageObject = ImageObject()
        if (tableView == self.imageTableView){
            imageObject = self.imageObjectsArray[indexPath.row]
        } else {
             imageObject = self.tagsImageObjectsArray[indexPath.row]
        }
        let imageCell = ImageTableViewCell.createForTableView(imageTableView) as! ImageTableViewCell
        imageCell.setTextImageNameLabel(imageObject.name)
        imageCell.setDateLabel(imageObject.date)
        imageCell.setTextTagsLabel(imageObject.tags)
        
        if (ImageManager.existInCache(imageObject.imageUrl)) {
            imageCell.setImage(image: ImageManager.getFromCache(imageObject.imageUrl))
        } else {
            ImageManager.loadImage(fromUrl: imageObject.imageUrl) { (image) in
                let imgSize = image.size
                let ratio = imgSize.height / imgSize.width
                let scaledHeight = tableView.frame.size.width * ratio
                self.imageObjectsArray[indexPath.row].imageHieght = scaledHeight
                self.imageTableView.reloadRows(at: [indexPath], with: .fade)
            }
        }
        
        return imageCell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(tableView == self.imageTableView){
            if (self.imageObjectsArray.count > indexPath.row) {
                return self.imageObjectsArray[indexPath.row].imageHieght
            } else {
                return self.hieghtCell
            }
        } else {
            if (self.tagsImageObjectsArray.count > indexPath.row) {
                return self.tagsImageObjectsArray[indexPath.row].imageHieght
            } else {
                return self.hieghtCell
            }
        }
    }
    
    
    func downloadImageList(updateTableView: UITableView) {
        let indicator = UIActivityIndicatorView()
        let buttonHeight = self.view.bounds.height
        let buttonWidth = self.view.bounds.size.width
        indicator.center = CGPoint(x: buttonWidth/2, y: buttonHeight/2)
        self.view.addSubview(indicator)
        indicator.startAnimating()

        DispatchQueue.global(qos: .background).async {
            RequestManager.sharedInstance.downloadImageList(tags: self.tags,
                                                            successBlock:
                ({ (imageObjects: Array<ImageObject>) -> Void in
                    if(updateTableView == self.imageTableView){
                        self.imageObjectsArray = imageObjects
                    } else {
                        self.tagsImageObjectsArray = imageObjects
                    }
                    DispatchQueue.main.async{
                        updateTableView.reloadData()
                        if(indicator.isAnimating)
                        {
                            indicator.stopAnimating()
                            indicator.removeFromSuperview()
                        }
                    }
                }),
                                                            errorBlock:
                ({ (er: Error) -> Void in
                    self.showErrorAlert(message: er.localizedDescription)
                    if(indicator.isAnimating)
                    {
                        indicator.stopAnimating()
                        indicator.removeFromSuperview()
                    }
                })
            )
        }
    }
    
    // MARK: segue section
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async() {
            [unowned self] in
            self.performSegue(withIdentifier: "image", sender: self)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "image" ,
            let nextScene = segue.destination as? ImageDetailsScreenViewController,
            let indexPath = self.imageTableView.indexPathForSelectedRow {
            let selectedImage = self.imageObjectsArray[indexPath.row]
            nextScene.imageObject = selectedImage
        }
    }

    // MARK: alert section
    
    func showErrorAlert(message: String){
        let alert = UIAlertController(title: "The request failed!", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "Repeat the request", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.downloadImageList(updateTableView: self.imageTableView)
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func showSortAlert() {
        let alert = UIAlertController(title: "Select a sorting type!", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        let sortDate = UIAlertAction(title: "Sort by date", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.sortDate()
            self.imageTableView.reloadData()
        }
        let sortName = UIAlertAction(title: "Sort by name", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.sortName()
            self.imageTableView.reloadData()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        
        alert.addAction(sortDate)
        alert.addAction(sortName)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}




