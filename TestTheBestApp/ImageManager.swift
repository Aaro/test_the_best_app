//
//  ImageManager.swift
//  TestTheBestApp
//
//  Created by Надежда Шальнова on 10.02.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

import UIKit

class ImageManager: NSObject {
    static private var imageCache = NSMutableDictionary()

    static func existInCache(_ url: String) -> Bool {
        return imageCache.object(forKey: url) != nil
    }
    
    static func getFromCache(_ url: String) -> UIImage {
        return imageCache.object(forKey: url) as! UIImage
    }
    
    static func loadImage(fromUrl url: String, withCompletion completion: @escaping (UIImage) -> Void) {
        DispatchQueue.global(qos: .background).async {
            var image = imageCache.object(forKey: url)
            if (image == nil) {
                image = UIImage()
                let imgURL = NSURL(string: url)!
                let imgData = NSData(contentsOf: imgURL as URL)
                if (imgData != nil) {
                    image = UIImage(data: imgData as! Data)!
                    imageCache.setValue(image, forKey: url)
                } else if (arc4random() % 2 == 1) {
                    image = UIImage(named: "fail1.jpeg")!;
                } else {
                    image = UIImage(named: "fail2.jpeg")!;
                }
            }
            DispatchQueue.main.async{
                completion(image as! UIImage)
            }
        }
    }
    
}
