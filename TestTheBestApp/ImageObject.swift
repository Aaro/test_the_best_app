//
//  ImageObject.swift
//  TestTheBestApp
//
//  Created by Надежда Шальнова on 08.02.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

import UIKit

class ImageObject: NSObject {
    var name: String = ""
    var date: Date = Date()
    var tags: String = ""
    var imageUrl : String = ""
    var imageHieght: CGFloat = 500.0
}
