//
//  ImageTableViewCell.swift
//  TestTheBestApp
//
//  Created by Надежда Шальнова on 08.02.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var myImageView: UIImageView!
    @IBOutlet private weak var dateImageLabel: UILabel!
    @IBOutlet private weak var imageNameLabel: UILabel!
    @IBOutlet private weak var tagsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func setTextImageNameLabel(_ name: String) {
        self.imageNameLabel.text = name
    }
    
    
    func setDateLabel(_ date: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        let dateString = dateFormatter.string(from: date)
        self.dateImageLabel.text = dateString
    }
    
    
    func setTextTagsLabel(_ tags: String) {
        self.tagsLabel.text = tags
    }
    
    
    func setImage(image: UIImage) {
        self.myImageView?.image = image
    }
    
}
