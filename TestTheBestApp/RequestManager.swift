//
//  RequestManager.swift
//  TestTheBestApp
//
//  Created by Надежда Шальнова on 08.02.17.
//  Copyright © 2017 Nadezhda Shalnova. All rights reserved.
//

import UIKit


typealias SuccessBlock = (Array<ImageObject>) -> Void
typealias ErrorBlock = (Error) -> Void


class RequestManager: NSObject {
    
    let urlPathBase = "https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1"
    
    
    static let sharedInstance : RequestManager = {
        let instance = RequestManager()
        return instance
    }()
    
    
    func downloadImageList(tags: String, successBlock:@escaping SuccessBlock, errorBlock: @escaping ErrorBlock) {
        var imageObjects = [ImageObject]()
        var urlPath = urlPathBase
        if(tags != "") {
            urlPath = self.appendTags(tags, toUrl: urlPath)
        }
        let req = NSMutableURLRequest(url: NSURL(string:urlPath) as! URL)
        req.httpMethod = "GET"
        URLSession.shared.dataTask(with: req as URLRequest) { data, response, error in
            if error != nil {
                errorBlock(error!)
            } else {
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:AnyObject] {
                        let list = jsonResult["items"] as! [[String: Any]]
                        imageObjects.removeAll()
                        for item in list {
                            imageObjects.append(self.createImageItem(item: item))
                        }
                        successBlock(imageObjects)
                    }
                } catch let error as NSError {
                    errorBlock(error)
                }
            }
            }.resume()
    }


    func appendTags(_ tags: String, toUrl urlPath:String)-> String {
       var newUrlPath = urlPath
       newUrlPath += "&tags="
       newUrlPath += tags
       return newUrlPath
    }
    
    
    func createImageItem(item: [String: Any]) -> ImageObject {
        let im: ImageObject = ImageObject()
        im.name = item ["title"] as! String
        im.date = self.formatTimeStringToDate(dateString: item ["published"] as! String)
        im.tags = item["tags"] as! String
        let imageAdress = item["media"] as! [String: Any]
        im.imageUrl = imageAdress["m"] as! String
        return im
    }
    
    
    func formatTimeStringToDate(dateString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZ"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        let dateObj = dateFormatter.date(from: dateString)
        return dateObj!
    }
    
}














